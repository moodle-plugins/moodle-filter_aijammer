<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * AI Jammer filter definition.
 * @package    filter_aijammer
 * @copyright  2024 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
global $CFG;
require_once($CFG->libdir . '/filterlib.php');

/**
 * Main class for AI Jammer filter.
 * @copyright  2024 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class filter_aijammer extends moodle_text_filter {

    /**
     * Filter adding jamming letters hidden with CSS.
     * @param string $text some HTML content to process.
     * @param array $options options passed to the filters
     * @return string the HTML content after the filtering has been applied.
     * @see moodle_text_filter::filter()
     */
    public function filter($text, array $options = []) {
        $map = [];
        foreach ([ 65, 97 ] as $offset) { // Uppercase and lowercase.
            foreach (range(0, 25) as $letter) {
                $obfuscation = implode('', array_map(function() use ($offset) {
                    return chr(rand(0, 25) + $offset);
                }, range(1, rand(1, 3))));
                $map[] = new filterobject(chr($letter + $offset),
                        '',
                        html_writer::span(
                                $obfuscation,
                                'nolink', [ 'style' => 'font-size:0' ]
                        ), true);
            }
        }
        $jammed = filter_phrases($text, $map);
        $jammed = html_writer::div($jammed, 'd-inline', [ 'aria-hidden' => 'true' ]); // Hide jammed text from screen readers.

        // Put original text for screen readers only.
        $srtext = preg_replace('/\[\[[^]]*\]\]/', '', s($text)); // Remove tags and placeholders to avoid duplicate elements.
        $srtext = html_writer::div($srtext, 'sr-only');

        return $jammed . $srtext;
    }
}
