## AI Jammer filter

This text filter is an attempt to make copy-pasting of online exams subjects into third-party tools (e.g. AI-based tools) more difficult.  
The filter will inject random letters within texts. These letters will be made invisible on pages with CSS, but will be present to jam copy-pasting or automatic page reading.

### Validation

This filter has been successfully reported to jam the following software:
- Quizwiz

It is meant to work - but has not yet been tested - on the following software:
- MoodleGPT

### Accessibility concerns

This filter scrambles HTML text without impacting normal page reading neither screen readers for visually-impaired users.  
Please address any issue you may encounter in this regard on the comments section or on the bug tracker URL.

### Installation and settings recommendations

When this plugin is first installed, it will be set in a "Off, but available" state. This is the recommended setting, as it will not be enabled throughout your site, but will be available to be enabled per-activity.  
It is recommended to apply this filter as one of the last filters, as it will break words in the HTML.  
It is also recommended to only apply this filter to content (not headings).  
The "Manage filters" admin page at <yourmoodle>/admin/filters.php allows to manage these settings.

### About

This software was developped with the Caseine project, with the support of Université Grenoble Alpes.