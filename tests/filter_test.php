<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Unit test for the AI Jammer filter.
 * @package    filter_aijammer
 * @copyright  2024 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace filter_aijammer;

use filter_aijammer, basic_testcase, context_system;

defined('MOODLE_INTERNAL') || die();
global $CFG;
require_once($CFG->dirroot . '/filter/aijammer/filter.php');

/**
 * Unit tests for filter_aijammer.
 * @covers \filter_aijammer::filter
 */
class filter_test extends basic_testcase {

    /**
     * @var filter_aijammer The filter we are testing.
     */
    protected $filter;

    protected function setUp() {
        parent::setUp();
        $this->filter = new filter_aijammer(context_system::instance(), []);
    }

    public function test_jammer() {
        $text = 'This text is to be jammed!';
        $after = $this->filter->filter('<div>' . $text . '</div>');

        $this->assertTrue(preg_match('/^<div [^>]*aria-hidden="?true"?/', $after) === 1, 'Starting aria-hidden div not found.');

        $delimiter = '<div class="sr-only">';
        $pos = strpos($after, $delimiter);

        $this->assertTrue($pos !== false, 'Screen reader div not found.');

        $jammed = strip_tags(substr($after, 0, $pos));
        $this->assertTrue(strpos($jammed, $text) === false, 'Text not jammed.');
        $this->assertTrue(strlen($jammed) > strlen($text), 'Text not jammed.');

        $srtext = strip_tags(substr($after, $pos));
        $this->assertEquals($text, $srtext, 'Text altered in screen reader div.');
    }
}
